import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
 
class ContactInformation extends React.Component{
    constructor(props){
         super(props);
         this.state={
            email:'',emailvalid:'',
            mobile:'',mobilevalid:'',
            adhar:'',adharvalid:''
            
         };   
    }
    adhar=(e)=>{
            e.preventDefault();
            if (e.target.value===''){
                this.setState({adharvalid:'*required'});
            }
            else if (!(/^[2-9]{1}[0-9]{3} [0-9]{4} [0-9]{4}$/).test(e.target.value)){
                this.setState({adharvalid:'*invalid format'});
            }
            else {
                this.setState({adharvalid:''});
            }
            
            let len=e.target.value.length;
            if (len===4||len===9){
                console.log(len);
                e.target.value+=" ";
            }
            this.setState({adhar:e.target.value});
    }
    email=(e)=>{
        e.preventDefault();
        this.setState({email:e.target.value});
        if (e.target.value===''){
            this.setState({emailvalid:'*required'});
        }
        else if (!(/\w+@\w+\.com/).test(e.target.value)){
            this.setState({emailvalid:'*invalid email'});
        }
        else {
            this.setState({emailvalid:''});
        }
    }
    mobile=(e)=>{
        e.preventDefault();
        this.setState({mobile:e.target.value});
        if (e.target.value===''){
            this.setState({mobilevalid:'*required'});
        }
        else if (!(/^\d{10}$/).test(e.target.value)){
            this.setState({mobilevalid:'*invalid mobile number'});
        }
        else {
            this.setState({mobilevalid:''});
        }
    }
    display=(e)=>
    {   
        e.preventDefault();
        this.props.history.push('/paymentdetails');
    }
    render(){
        let {email,emailvalid,mobile,mobilevalid,adhar,adharvalid}=this.state;
        return(
            <div>
                <br/><br/><br/>
                    <center>
                        <form>
                            <div className="contactdetails">
                                <h3 className="hh">Contact Information</h3>
                                <br/>
                                <center>
                                    <table>
                                        <tr>
                                            <td align="right"><label className="ff">E-Mail:</label></td>
                                            <td align="left"><input type="email" className="email" placeholder="example@gmail.com" value={email} onChange={this.email}/></td>
                                        </tr>
                        
                                        <tr>
                                            <td></td>
                                            <td><p className="pp">{emailvalid}</p></td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="right"> <label className="ff">Mobile Number:</label></td>
                                            <td align="left"><input type="number" placeholder="Mobile Number" className="mobile" value={mobile} onChange={this.mobile}/></td>
                                        </tr>
                                            
                                        <tr>
                                            <td></td>
                                            <td><p className="pp">{mobilevalid}</p></td>
                                        </tr>
                                    </table>
                                </center>
                            </div>
                            <br/>
                            <div className="idproof">
                                <h3 className="hh">Id Proof verification</h3>
                                <br/>
                                <center>
                                    <table>
                                        <tr>
                                            <td align="right"><label className="ff">Aadhar No:</label></td>
                                            <td align="left"> <input type="text" className="adhar" placeholder="Aadhar No"  value={adhar} onChange={this.adhar}/></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><p className="pp">{adharvalid}</p></td>
                                        </tr>
                                    </table>
                                </center>
                                <br/><br/><br/>
                                <Button type="submit" onClick={this.display} variant="success">Continue Booking</Button>
                                <br/>
                            </div>
                        </form>
                    </center>
            </div>
        )
    }
}
 export default ContactInformation;