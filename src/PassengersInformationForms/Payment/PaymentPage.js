import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';

class PaymentPage extends React.Component{
    constructor(props){
        super(props);
        this.state={
            holdername:'',holdernamevalid:'',
            bankname:'',banknamevalid:'',
            creditcard:'',creditcardvalid:'',
            cvv:'',cvvvalid:''
        };
    }
    holdername=(e)=>
    {
        this.setState({holdername:e.target.value});
        if (e.target.value===''){
            this.setState({holdernamevalid:'*required'});
        }
        else if (!(/^[a-z\s]+$/i.test(e.target.value))){
             this.setState({holdernamevalid:'only alphabets'});
        } 
        else {
             this.setState({holdernamevalid:''});
        }  
    }
    bankname=(e)=>{
        this.setState({bankname:e.target.value});
        if (e.target.value===''){
           this.setState({banknamevalid:'*required'});
        }
        else if (!(/^[a-z\s]+$/i.test(e.target.value))){
            this.setState({banknamevalid:'only alphabets'});
        } 
        else {
             this.setState({banknamevalid:''});
        }
    }
    creditcard=(e)=>{
        e.preventDefault();
        if (e.target.value===''){
            this.setState({creditcardvalid:'*required'});
        }  
        else if (!(/^\d{4}-\d{4}-\d{4}-\d{4}$/.test(e.target.value))){
            this.setState({creditcardvalid:'*invalid format'});
        }
        else {
            this.setState({creditcardvalid:''});
        }
        
        let cre=e.target.value.length;
        if (cre===4||cre===9||cre===14){
            e.target.value+="-";
        }
        this.setState({creditcard:e.target.value});
    }
    cvv=(e)=>{
        this.setState({cvv:e.target.value});
        if (e.target.value===''){
            this.setState({cvvvalid:'*required'});
        }
        else if (!(/^\d{3}$/.test(e.target.value))){
            this.setState({cvvvalid:'*invalid number'});
        }
        else {
            this.setState({cvvvalid:''});
        }
    }
            

    display=(e)=>{   
        e.preventDefault();
        this.props.history.push('/ending');
    }
    render(){
        let {holdername,holdernamevalid,bankname,banknamevalid,creditcard,creditcardvalid,cvv,cvvvalid}=this.state;
        return(
            <div>
                <br/><br/><br/>
                <center>
                    <form>
                        <div className="paymentdetails">
                            <h3 className="hh">Payment Details</h3>
                            <br/>
                            <center>
                                <table>
                                    <tr>
                                        <td align="right"> <label className="ff">Card Holder Name:</label></td>
                                        <td align="left"><input type="text" placeholder="Card Holder Name" value={holdername} onChange={this.holdername}/></td>
                                    </tr>
                        
                                    <tr>
                                        <td></td>
                                        <td><p className="pp">{holdernamevalid}</p></td>
                                    </tr>

                                    <tr>
                                        <td align="right"> <label className="ff">Bank Name:</label></td>
                                        <td align="left"><input type="text" placeholder="Bank Name" value={bankname} onChange={this.bankname}/></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td> <p className="pp">{banknamevalid}</p></td>
                                    </tr>
                           
                                    <tr>
                                        <td align="right"><label className="ff">Credit Card No:</label></td>
                                    <td align="left"> <input type="text" placeholder="Credit card Number" value={creditcard} onChange={this.creditcard}/></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td><p className="pp">{creditcardvalid}</p></td>
                                    </tr>
                                                                
                                    <tr>                        
                                        <td align="right"><label className="ff">CVV No:</label></td>
                                        <td align="left"><input type="number" className="cvv" placeholder="CVV No" value={cvv} onChange={this.cvv}/></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td> <p className="pp">{cvvvalid}</p></td>
                                    </tr>
                                </table>
                            </center>
                            <br/>
                            <Button type="submit" onClick={this.display} variant="success">Confirm</Button>
                        </div>
                    </form>
                </center>
            </div>
        )
    }
}
export default PaymentPage;