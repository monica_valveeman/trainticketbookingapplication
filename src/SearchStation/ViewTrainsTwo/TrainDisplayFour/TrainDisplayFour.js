import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Spinner,Button} from 'react-bootstrap';

class TrainDisplayFour extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Traindetails: [],
            isLoading:true,
        }    
    }
    componentDidMount() {
        axios.get(`https://run.mocky.io/v3/a382266b-d9d3-4806-ac0c-07725cc55393`)
          .then(res => {
            console.log(res);
            this.setState({ Traindetails : res.data,isLoading:false});
          })
    }
    avail=()=>{
        this.props.history.push('/traindisplaythree');
    }
    avail1=()=>{
        this.props.history.push('/traindisplayfour');
    }
    render(){
        return(
            <center>
                <br/><br/>
                <div>
                    <td>    
                        <Container className="firsttrains">
                            {this.state.Traindetails.map(trainnames=>
                                <pre className="pretag"><b className="trainame">{trainnames.atname}</b><br/>
                                <b>Train No:</b> {trainnames.atno}                     <b> {trainnames.aFrom}................{trainnames.aTo}</b>
                                <br/>                                          <strong>{trainnames.aTimefrom}                      {trainnames.aTimeto}</strong>     <Button variant="dark" type="submit" disable={this.state.disable} onClick={this.avail}>Check Availability</Button></pre>)}
                                <hr/>
                        </Container>
                    </td>
                    <br/>
                    {this.state.isLoading?<Spinner animation="border" />:
                        <td>    
                            <Container className="firsttrains">
                                {this.state.Traindetails.map(trainnames=>
                                    <pre className="pretag"><b className="trainame">{trainnames.btname}</b><br/>
                                    <b>Train No:</b> {trainnames.btno}                     <b> {trainnames.bFrom}................{trainnames.bTo}</b>
                                    <br/>                                          <strong>{trainnames.bTimefrom}                      {trainnames.bTimeto}</strong>       <Button variant="dark" type="submit" disable={this.state.disable} onClick={this.avail1}>Check Availability</Button></pre>)} 
                                    <hr/>
                                    <Link className="link4" to='/secondaryseatthree'>Secondary Seat</Link>{'  '}
                                    <Link className="link5" to='/ac3tierthree'> AC 3 Tier</Link>{'  '}
                            </Container>
                        </td>}
                </div>
            </center>
        )
    }
}
export default TrainDisplayFour;
