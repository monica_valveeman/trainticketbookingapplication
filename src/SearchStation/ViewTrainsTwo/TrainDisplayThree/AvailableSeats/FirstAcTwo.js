import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Card,Spinner,Button,Row} from 'react-bootstrap';

class FirstAcTwo extends React.Component{
    constructor(props){
        super(props);
        this.state={
            Train1:[],
            isLoading:true
        }    
    }
    componentDidMount(){
        axios.get(`https://run.mocky.io/v3/914fde09-7191-4cd2-a66c-a267c4ea4609`)
        .then(res=>{
            console.log(res);
            this.setState({Train1: res.data,isLoading:false});
         } )
    }
    details=(e)=>{
        e.preventDefault();
        this.props.history.push('/passengerdetails');
    }
    goback=(e)=>{
        e.preventDefault();
        this.props.history.push('/traindisplaythree');
    }
    render(){
        return(
            <div>
                <br/><br/>
                <center>
                    {this.state.isLoading?<Spinner animation="border" />:
                        <Container className="rr">
                            <p>Train Name: Tn Ms Exp</p>
                            <p>Train No: 02694</p>
                            <p> <Button variant="primary" onClick={this.goback}>Goback</Button></p>
                            <td>
                                {this.state.Train1.map(train=>
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.faf.gdate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.faf.gseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.faf.gamt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}</td><td>
                                
                                {this.state.Train1.map(train=>                
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.fas.hdate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.fas.hseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.fas.hamt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}</td><td>
                            
                                {this.state.Train1.map(train=>                    
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.fat.idate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.fat.iseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.fat.iamt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}                   
                            </td>
                        </Container> 
                    }
                </center>                
            </div>
        )
    }
}
export default FirstAcTwo;