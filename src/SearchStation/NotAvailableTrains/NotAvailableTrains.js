import React from 'react';
import {HiOutlineEmojiSad} from 'react-icons/hi';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';

class NotAvailableTrains extends React.Component{
    constructor(props){
        super(props);
    }
    tryagain=(e)=>{
        e.preventDefault();
        this.props.history.push("/searchtrains");
    }
    render(){

        return(
            <div>
                <br/><br/>
                <div>
                    <center>
                        <br/><br/><br/>
                        <h1>Trains are NOT AVAILABLE....</h1>
                        <h1><HiOutlineEmojiSad/></h1>
                        <Button variant="success" type="submit" onClick={this.tryagain}>Try Again</Button>
                    </center>
                </div>
            </div>
        )
    }
}
export default NotAvailableTrains;