import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Spinner,Button} from 'react-bootstrap';

class ViewTrainsOne extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Traindetails: [],
            isLoading:true,
        }    
    }
    componentDidMount() {
        axios.get(`https://run.mocky.io/v3/4d3c6834-62be-4775-bfcf-649101cf78b6`)
          .then(res => {
            console.log(res);
            this.setState({ Traindetails : res.data, isLoading:false});
          })
    }
    avail=(e)=>{
        e.preventDefault();
        this.props.history.push('/traindisplayone');
    }
    avail1=()=>{
        this.props.history.push('/traindisplaytwo');
    }

    render(){
        return(
            <center>
                <br/><br/>
                <div>
                    {this.state.isLoading?<Spinner animation="border" />:
                        <td>    
                            <Container className="firsttrains">
                                {this.state.Traindetails.map(trainnames=>
                                    <pre className="pretag"><b className="trainame">{trainnames.atname}</b><br/>
                                    <b>Train No:</b> {trainnames.atno}                     <b> {trainnames.aFrom}................{trainnames.aTo}</b>
                                    <br/>                                          <strong>{trainnames.aTimefrom}                      {trainnames.aTimeto}</strong>       <Button variant="dark" type="submit" disable={this.state.disable} onClick={this.avail}>Check Availability</Button></pre>)}
                                    <hr/>
                            </Container>
                        </td>}
                        <br/>
                        <td>    
                            <Container className="firsttrains">
                                {this.state.Traindetails.map(trainnames=>
                                    <pre className="pretag"><b className="trainame">{trainnames.btname}</b><br/>
                                    <b>Train No:</b> {trainnames.btno}                     <b> {trainnames.bFrom}................{trainnames.bTo}</b>
                                    <br/>                                          <strong>{trainnames.bTimefrom}                      {trainnames.bTimeto}</strong>       <Button variant="dark" type="submit" disable={this.state.disable} onClick={this.avail1}>Check Availability</Button></pre>)}
                                    <hr/>
                            </Container>
                        </td>
                </div>
            </center>

        )
    }
}
export default ViewTrainsOne;
