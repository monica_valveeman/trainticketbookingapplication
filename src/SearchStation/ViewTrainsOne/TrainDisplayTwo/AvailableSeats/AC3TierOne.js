import React from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Card,Spinner,Button,Row} from 'react-bootstrap';

class AC3TierOne extends React.Component{
    constructor(props){
        super(props);
        this.state={
            Train2:[],
            isLoading:true
        }    
    }
    componentDidMount(){
        axios.get(`https://run.mocky.io/v3/d65152db-c493-41ab-b4eb-fd18a090b46c`)
        .then(res=>{
            console.log(res);
            this.setState({Train2: res.data,isLoading:false});
         } )
    }
    details=(e)=>{
        e.preventDefault();
        this.props.history.push('/passengerdetails');
    }
    goback=(e)=>{
        e.preventDefault();
        this.props.history.push('/traindisplaytwo');
    }
    render(){
        return(
            <div>
                <br/><br/>
                <center>
                    {this.state.isLoading?<Spinner animation="border" />:
                        <Container className="rr">
                            <p>Train Name: Ms Guruvayur Ex </p>
                            <p>Train No: 06127</p>
                            <p> <Button variant="primary" onClick={this.goback}>Goback</Button></p>
                            <td>
                                {this.state.Train2.map(train=>
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.ac3f.gdate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.ac3f.gseat} 
                                            </Card.Text>
                                            <Card.Text>
                                                {train.ac3f.gamt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}</td><td>
                            
                                {this.state.Train2.map(train=>
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.ac3s.hdate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.ac3s.hseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.ac3s.hamt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}</td><td>
                                {this.state.Train2.map(train=>
                                    <Card className="cards">
                                        <Card.Body>
                                            <Card.Text>
                                                {train.ac3t.idate}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.ac3t.iseat}
                                            </Card.Text>
                                            <Card.Text>
                                                {train.ac3t.iamt}
                                            </Card.Text>
                                        </Card.Body>
                                        <Button variant="secondary" type="submit" onClick={this.details}>Book</Button>
                                    </Card>
                                )}
                            </td>
                        </Container> 
                    }
                </center>
            </div>
        )
    }
}
export default AC3TierOne;