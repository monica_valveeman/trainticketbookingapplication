import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import smiley from './smiley.png';

class LinkPage extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isLoad:true
        }
    }
    componentDidMount(){
        this.setState({isLoad:false});
    }
    Search=(e)=>
    {
        e.preventDefault();
       
        this.props.history.push('/searchtrains');
    }
    render(){
        if (this.props.ticket.Traindetails===undefined){
        console.log(this.props.ticket.apicall);
        }
        return(
            <center>
            <div>
                <br/><br/>
                <h1>Welcome,{this.props.passenger.username}</h1>
                <form onSubmit={(e)=>this.Search(e)}>
                    <Button type="submit"  variant="dark">Click to Book Ticket</Button>
                </form>
                <h1>Happy Journey....</h1><img src={smiley} alt='smiley'/>
            </div>
            </center>
        )
    }
}

function mapStatetoProps(state){
    return{
        passenger:state,
        ticket:state
    
    }
}
export default connect (mapStatetoProps)(LinkPage)
