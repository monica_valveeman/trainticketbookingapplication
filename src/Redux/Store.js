import {createStore} from 'redux';
import useReducer from './useReducer';
export const store=createStore(useReducer);
