import React from 'react';
import {GrEmoji} from 'react-icons/gr';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';

class EndPage extends React.Component{

    again=(e)=>{
        e.preventDefault();
        this.props.history.push('/searchtrains');
    }
    render(){
        return(
            <div>
                <br/><br/>
                <div>
                    <center><br/><br/><br/><br/>
                    <h1>Thank You for Booking Your Ticket</h1>
                    <h3>Have a Safe Journey</h3>
                    <h1><GrEmoji/></h1>
                    <h5>If you are interested, Click the button to Book your ticket Again</h5>
                    <Button type="submit" variant="danger" onClick={this.again}>Booking Again</Button>
                    </center>
                </div>
            </div>
        )
    }
}
export default EndPage;